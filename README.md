# JCC GitFlow

## Sceenshoot GitFlow Usage

1. Initial GitFlow

```bash
git flow init
```

![GitFlow Init](assets/screenshoot/gitflow-1.png)

2. Create Branch Feature

```bash
git flow feature start screenshoot
```

![GitFlow Feature Start](assets/screenshoot/gitflow-2.png)

3. Finish Feature Branch & Push to Develop Branch

```bash
git flow feature finish screenshoot
git push -u origin develop
```

![GitFlow Feature Finish](assets/screenshoot/gitflow-3.png)

4. Create Merge Request
   Go to URL Project and click `Create Merge Request`
   ![GitFlow Merge Request](assets/screenshoot/gitflow-4.png)

5. Merge to Main Branch
   Click `Merge` to merge Develop Branch to Main Branch
   ![GitFlow Main Branch](assets/screenshoot/gitflow-5.png)
